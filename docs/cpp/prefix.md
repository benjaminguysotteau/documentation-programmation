Préfix
======

| Préfix | Désignation           |
| :----- | :-------------------- |
| C      | Class or not useful?  |
| a      | Array                 |
| ba     | Byte array            |
| bl     | Bool                  |
| ctr    | Counter               |
| dbg    | Debug                 |
| def    | Define                |
| flg    | Flag                  |
| idx    | Index                 |
| m      | Map                   |
| p      | Pointeur              |
| r      | Référence             |
| s      | Struct                |
| sgn    | Signal                |
| slt    | Slot                  |
| sp     | Serial port           |
| sql    | SQL Query             |
| str    | String                |
| thr    | Thread                |
| tmr    | Timer                 |
| tr     | Translate             |
| u16    | Unsigned char 16 bits |
| u32    | Unsigned char 32 bits |
| u64    | Unsigned char 64 bits |
| uc     | Unsigned char         |
| ui     | Unsigned int          |
| v      | Vector                |

Référence
---------

1.[Bonnes pratiques Langage C](https://emmanuel-delahaye.developpez.com/tutoriels/c/bonnes-pratiques-codage-c/)
