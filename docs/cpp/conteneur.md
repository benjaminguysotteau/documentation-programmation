Collections
=

!!! info
	Une <ins>collection</ins> est l'association d'un **conteneur** et d'un **template**.

Séquences
-


| Dénomination   | C++                     | C#             | Unity    |
| :--            | :--                     | :--            | :--      |
| vector         | std::vector<T\>         | List<T\>       | List<T\> |
| vector trié    | std::vector<T\>         | SortedList<T\> |          |
| deque          | std::deque<T\>          |                |          |
| list           | std::list<T\>           | LinkedList<T\> |          |
| stack          | std::stack<T\>          | Stack<T\>      |          |
| queue          | std::queue<T\>          | Queue<T\>      |          |
| priority_queue | std::priority_queue<T\> |                |          |


___


Conteneurs associatifs
-


| Dénomination  | C++                            | C#                              | Unity |
| :--           | :--                            | :--                             |       |
| set           | std::set<Key\>                 | SortedSet<T\>                   |       |
| multiset      | std::multiset<Key, Data\>      |                                 |       |
| unordered_set | std::unordered_set<Key\>       | HashSet<T\>                     |       |
| map           | std::map<Key, Data\>           | SortedDictionary<TKey, TValue\> |       |
| multimap      | std::multimap<Key, Data\>      |                                 |       |
| unordered_map | std::unordered_map<Key, Data\> | Dictionary<TKey, TValue\>       |       |


Référence
-

1. [Stackoverflow - Comparison C++ STL collections and C Sharp Collections](https://stackoverflow.com/questions/3659044/comparison-of-c-stl-collections-and-c-sharp-collections)
