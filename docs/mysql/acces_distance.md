Accès distance
===

Afficher les **ports reverse** ssh pour les <ins>tous les terminaux</ins> depuis la base de données.

---

Se [connecter à la base de données](connection.md#server-ovh) hébergée sur le serveur OVH.


Utiliser la table **Heurtauxv01**.

```sql
mysql> USE heurtauxv01;
```

---

Émettre la requête pour **monitoring**:

```sql
SELECT station_id, machine_address, value, visible, date FROM monitoring WHERE parameter_id = 474;
```

!!! Warning
	**parameter_id = 474** est la valeur en dure pour le port reverse ssh.
	
	Le faire avec une pseudo variable à la place.

??? info "Exemple: Résultat sortie standard"
	```sql
	+------------+-----------------+-------+---------+---------------------+
	| station_id | machine_address | value | visible | date                |
	+------------+-----------------+-------+---------+---------------------+
	|          1 |             127 | 0     |       1 | 0000-00-00 00:00:00 |
	|          2 |             127 | 0     |       1 | 2017-05-31 20:47:24 |
	|          3 |             127 | 0     |       1 | 2017-06-01 04:14:05 |
	|          4 |             127 | 0     |       1 | 2017-06-01 04:36:44 |
	|          5 |             127 | 0     |       1 | 2017-06-03 08:24:03 |
	|          6 |             127 | 22180 |       1 | 2010-01-01 00:00:00 |
	|          7 |             127 | 0     |       1 | 2017-05-31 21:41:39 |
	|          8 |             127 | 0     |       1 | 2017-06-01 04:44:45 |
	|          9 |             127 | 0     |       1 | 2017-05-31 22:09:08 |
	|         15 |             127 | 0     |       1 | 2017-06-08 13:13:30 |
	|         10 |             127 | 0     |       1 | 2017-06-01 05:39:27 |
	|         11 |             127 | 0     |       1 | 2017-05-31 21:46:42 |
	|         12 |             127 | 0     |       1 | 0000-00-00 00:00:00 |
	|         13 |             127 | 23317 |       1 | 2018-03-29 13:41:41 |
	|         14 |             127 | 0     |       1 | 2017-06-01 04:34:04 |
	|         16 |             127 | 0     |       1 | 2010-01-01 00:00:00 |
	|         17 |             127 | 0     |       1 | 2017-06-01 04:13:25 |
	|         18 |             127 | 0     |       1 | 2017-05-05 07:44:17 |
	|         19 |             127 | 0     |       1 | 0000-00-00 00:00:00 |
	|         23 |             127 | 0     |       1 | 2018-05-31 15:11:48 |
	|         24 |             127 | 22180 |       1 | 2018-06-08 05:35:26 |
	+------------+-----------------+-------+---------+---------------------+
	```
