Type paramêtre
===

Afficher le dernier type paramêtre depuis la base de données pour obtenir le numéro du code à utiliser pour Qt.

---

Se [connecter à la base de données](connection.md#server-ovh) hébergée sur le serveur OVH.


Utiliser la table **optiweb**.

```sql
mysql> USE optiweb;
```

---

Émettre la requête pour **optiweb.event_types**:

```sql
SELECT name, event_code, description, is_maintenance, is_default, send_alert FROM optiweb.event_types order by CONVERT(event_code, INTEGER) desc limit 1;
```

??? info "Exemple: Résultat sortie standard"
	```sql
	+-------------------------------------+------------+---------------------------------------------------------------------------------------+----------------+------------+------------+
	| name                                | event_code | description                                                                           | is_maintenance | is_default | send_alert |
	+-------------------------------------+------------+---------------------------------------------------------------------------------------+----------------+------------+------------+
	| Hopper jetons 2 erreur distribution | 337        | Une erreur est apparue durant la distribution des jetons, plus de jetons ou en panne. |              0 |          1 |          0 |
	+-------------------------------------+------------+---------------------------------------------------------------------------------------+----------------+------------+------------+
	1 row in set (0.00 sec)
	```
