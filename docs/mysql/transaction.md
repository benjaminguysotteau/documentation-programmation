Transaction
===========

Afficher les **transactions** pour une <ins>période donnée</ins> depuis la base de données.
___

Se [connecter à la base de données](connection.md#centrale) correspondante à la station souhaité.


Utiliser la table **Heurtauxv01**.

```sql
mysql> USE heurtauxv01;
```

Émettre la requête pour external_transac:

```sql
--mysql> 
SELECT id, reg_id, produit_id, income, libelle, end_date, updated_date, is_deleted, numero_cb, autorisation_cb, ext_transac_num
FROM external_transac WHERE is_deleted=0
AND  begin_date>='2018-08-28 14:00:00' AND end_date<='2018-08-28 18:00:00';
```

Resultat sortie standard:

```sql
+------+--------+------------+--------+------------------------+---------------------+---------------------+------------+-----------+-----------------+-----------------+
| id   | reg_id | produit_id | income | libelle                | end_date            | updated_date        | is_deleted | numero_cb | autorisation_cb | ext_transac_num |
+------+--------+------------+--------+------------------------+---------------------+---------------------+------------+-----------+-----------------+-----------------+
| 3919 |      0 |         13 |   2000 | Vente carte par CB     | 0000-00-00 00:00:00 | 2018-08-30 08:16:30 |          0 | NULL      | NULL            |  20180830081556 | 
| 4802 |      0 |         15 |   2000 | Recharge carte par CB  | 0000-00-00 00:00:00 | 2018-09-14 10:16:27 |          0 | NULL      | NULL            |  20180914101611 | 
| 6409 |      1 |          0 |    500 | NULL                   | 0000-00-00 00:00:00 | 2018-10-19 13:59:20 |          0 | NULL      | NULL            |  20181019135900 | 
| 6410 |      1 |          0 |    500 | NULL                   | 0000-00-00 00:00:00 | 2018-10-19 14:03:15 |          0 | NULL      | NULL            |  20181019140301 | 
| 6411 |      1 |          0 |    500 | NULL                   | 0000-00-00 00:00:00 | 2018-10-19 14:06:51 |          0 | NULL      | NULL            |  20181019140634 | 
| 6412 |      1 |          0 |    500 | NULL                   | 0000-00-00 00:00:00 | 2018-10-19 14:11:16 |          0 | NULL      | NULL            |  20181019141107 | 
| 6413 |      1 |          0 |    500 | NULL                   | 0000-00-00 00:00:00 | 2018-10-19 14:18:37 |          0 | NULL      | NULL            |  20181019141818 | 
| 6414 |      1 |          0 |    500 | NULL                   | 0000-00-00 00:00:00 | 2018-10-19 14:19:48 |          0 | NULL      | NULL            |  20181019141818 | 
| 6415 |      1 |          0 |    500 | NULL                   | 0000-00-00 00:00:00 | 2018-10-19 14:42:16 |          0 | NULL      | NULL            |  20181019144206 | 
| 6416 |      1 |          0 |    500 | NULL                   | 0000-00-00 00:00:00 | 2018-10-19 14:46:42 |          0 | NULL      | NULL            |  20181019144629 | 
| 6417 |      1 |          0 |    500 | NULL                   | 0000-00-00 00:00:00 | 2018-10-19 14:50:03 |          0 | NULL      | NULL            |  20181019144953 | 
| 6563 |      1 |          0 |    500 | NULL                   | 0000-00-00 00:00:00 | 2018-10-21 15:17:01 |          0 | NULL      | NULL            |  20181021151639 | 
| 6564 |      1 |          0 |    500 | NULL                   | 0000-00-00 00:00:00 | 2018-10-21 15:18:59 |          0 | NULL      | NULL            |  20181021151851 | 
| 6682 |      0 |         11 |    500 | Vente de jetons par CB | 0000-00-00 00:00:00 | 2018-10-25 14:46:36 |          0 | NULL      | NULL            |  20181025144552 | 
| 6709 |      1 |          0 |    500 | NULL                   | 0000-00-00 00:00:00 | 2018-10-26 09:47:26 |          0 | NULL      | NULL            |  20181026094658 | 
| 6710 |      1 |          0 |    500 | NULL                   | 0000-00-00 00:00:00 | 2018-10-26 09:56:29 |          0 | NULL      | NULL            |  20181026095617 | 
| 6784 |      0 |         15 |   3000 | Recharge carte par CB  | 0000-00-00 00:00:00 | 2018-10-27 13:06:02 |          0 | NULL      | NULL            |  20181027130507 | 
| 7306 |      0 |         13 |   2000 | Vente carte par CB     | 0000-00-00 00:00:00 | 2018-11-09 16:30:42 |          0 | NULL      | NULL            |  20181109163006 | 
+------+--------+------------+--------+------------------------+---------------------+---------------------+------------+-----------+-----------------+-----------------+
```

+ **reg_id**:     type de réglèment.
+ **produit_id**: type de vente.
+ **income**:     montant de la transaction.
    + **13**: vente carte par CB.
    + **15**: recharge carte par CB.
+ **is_deleted**: l'écriture comptable a été rapproché.
+ **ext_transac_num**: est composé de la date du jour et de l'heure.
