Connection
==========

Description des processus de connection à la **base de données** d'une <ins>centrale</ins> en production ou de la **base de données** du <ins>serveur OVH</ins>.
___

Centrale
--------

Procédure de connection à la **base de données** d'une <ins>centrale</ins>.

Depuis [Optiweb](https://www.optiweb-access.fr/login) (<ins>version en production</ins>):

Se loguer à l'aide de son **mail** appartement au domaine HEURTAUX (**@heurtaux.fr**) et de son **mot de passe**.

![Optiweb beta connection](img/optiweb_beta_connection.png)

Dans les onglets, cliquer sur **Mes Machines**.

![Optiweb beta mes machines](img/optiweb_beta_mes_machines.png)

Dans le **menu de gauche** (<ins>gestion globale</ins>), cliquer sur la centrale appartenant à la station.

1. Bouton **Paramètres**.
2. Section **Système**.
3. Champ **Enable SSH pour debug**.
4. S'assurer que la valeur du champ vaut **1**.
5. `Si la valeur du champ vaut 0`, cliquer sur **Modifier** dans la colonne **Actions**.

![Optiweb beta ssh enable](img/optiweb_beta_ssh_enable.png)

1. `Si la valeur du champ vaut 0`, depuis le menu déroulant passer le champ **valeur modifiée** à **Oui**.
2. `Si la valeur du champ vaut 0`, cliquer sur le bouton **Sauvegarder**.

![Optiweb beta ssh value true](img/optiweb_beta_ssh_value_true.png)

Afficher la **valeur** du port reverse ssh.

11. Bouton **Compteur**.
12. Section **Système**.
13. Champ **Port reverse SSH**.
14. Copier la **nouvelle valeur** du port reverse SSH. (<ins>rafraichir la page internet jusqu'à la mise à jour de la valeur</ins>).

![Optiweb beta ssh reverse port](img/optiweb_beta_ssh_reverse_port.png)

??? info "l'url contient l'id station et l'id machine"
    https://www.optiweb-access.fr/mes-machines/machine/22-127

    L'url du navigateur internet contient dans sa dernière partie l'**id de la station** qui est dans cet exemple <ins>22</ins> et celui de la **machine** sélectionnée qui est <ins>127</ins>.

    - <ins>127</ins> est égale à une **centrale**.
    - <ins> 49</ins> est égale à un **distributeur**.


Une fois le **port ssh actualisé** dans la partie **compteur** de la centrale.

Se connecter au <ins>serveur OVH</ins>.

```sh
# Depuis la vm de Benjamin.
ssh ovh-server

# Depuis la vm d'Alexandre.
lockovh
```

puis sur la <ins>centrale</ins>.

```sh
# lock <numero-port-reverse-ssh>
lock 22100
```

**Se connecter** à la base donnees en tant que <ins>root</ins>:

```sh
mysql -p<password>
```

La procédure de connection à la **base de données** d'une <ins>centrale</ins> est terminée.
___

Serveur OVH
-----------

Procédure de connection à la **base de données** du <ins>serveur OVH</ins>.

Ajouter la clé rsa à l'agent ssh d'Ubuntu.

```sh
ssh-add ~/.ssh/vm_phyvm_mysql_db_benjaminguysotteau
```

Se connecter en ssh via l'utilisateur readonlybdd à la base données OVH.

```sh
ssh ovh-mysql
```

Entrer le mot de passe de l'utilisateur pour se connecter.

```sh
mysql -p<password>
```

La procédure de connection à la **base de données** du <ins>serveur OVH</ins> est terminée.
