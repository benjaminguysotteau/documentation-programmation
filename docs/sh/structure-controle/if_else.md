If else
===

Multilines
---

```sh
directory_w_year_n_day="06-station-de-test/2018/23"

if [ ! -d "$directory_w_year_n_day" ]; then
	mkdir -p "$directory_w_year_n_day"
fi
```
___

Une ligne
---

```sh
directory_w_year_n_day="06-station-de-test/2018/23"

if [ ! -d "$directory_w_year_n_day" ] then; mkdir -p "$directory_w_year_n_day"; fi
```

___


### Factorisation

```sh
directory_w_year_n_day="06-station-de-test/2018/23"

[ ! -d "$directory_w_year_n_day" ] && mkdir "$directory_w_year_n_day"
```
