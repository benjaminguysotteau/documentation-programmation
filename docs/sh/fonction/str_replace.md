str_replace
===========

Fonction de **remplacement d'un charactère** par un autre <ins>dans une chaîne</ins>.

Bash - Parameter expansion
--------------------------

Remplacer tous les **x**, **y**, **z** par un <ins>underscore</ins>.

### Remplacer première occurence

```sh
str_w_char_to_replace="AxxBCyyyDEFzzLMN"

str_w_replaced_char=${str_w_char_to_replace/[xyz]/_}
```

### Remplacer toutes occurences

!!! tip
    Pour remplacer toutes les occurences, il suffit de <ins>doubler</ins> le **premier slash** du pattern.

```sh
str_w_char_to_replace="AxxBCyyyDEFzzLMN"

str_w_replaced_char=${str_w_char_to_replace//[xyz]/_}
```

___

Via sed
-------

### Remplacer un charactère

Remplacer tous les **x**, par un <ins>underscore</ins>.

```sh
str_w_char_to_replace="AxxBCyyyDEFzzLMN"

sed -i 's/x/_/g' $str_w_char_to_replace
```

### Remplacer plus d'un charactère

Remplacer tous les **x**, **y**, **z** par un <ins>underscore</ins>.

```sh
str_w_char_to_replace="AxxBCyyyDEFzzLMN"

sed -i 's/[xyz]+/_/g' $str_w_char_to_replace
```

___

Via tr
------

### Remplacer un charactère

Remplacer tous les **x**, par un <ins>underscore</ins>.

```sh
str_w_char_to_replace="AxxBCyyyDEFzzLMN"

tr 'x' '_' $str_w_char_to_replace
```

### Remplacer plus d'un charactère

Remplacer tous les **x**, **y**, **z** par un <ins>underscore</ins>.

```sh
str_w_char_to_replace="AxxBCyyyDEFzzLMN"

tr '[xyz]' '_' $str_w_char_to_replace
```

