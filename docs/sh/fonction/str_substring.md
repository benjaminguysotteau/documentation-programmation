str_substring
===

Équivalent de la fonction <ins>str_substring</ins> de **C** en **shell**.


Bash version
---

### Valeurs constantes 

On commence l'extraction à la **position 12** depuis le <ins>début de la chaîne de caratères</ins>

et on extrait les **5 caractères contigus** depuis l'<ins>offset</ins> de 12.

```sh
# Syntaxe: ${<nom-variable>:<offset-debut>:<longueur>}

str_substring=${str_full:12:5}
```

___


### Suppression début de chaîne

On <ins>supprime</ins> le contenu **avant la première occurence** par rapport au caractère `_`.

```sh
# Syntaxe: ${<nom-variable>#*<caractère-recherché>}

str_substring=${str_full#*_}   # remove prefix ending in "_"
```

___


### Suppression fin de chaîne

On <ins>supprime</ins> le contenu **après la dernière occurence** par rapport au caractère `_`.

```sh
# Syntaxe: ${<nom-variable>%<caractère-recherché>*}

str_substring=${str_full%_*}   # remove suffix starting with "_"
```

___



Référence
---

1. [Stackoverflow - Extract substring in bash](https://stackoverflow.com/questions/428109/extract-substring-in-bash)
