Objectif
========

Documentation qui est composée des points fondamentaux à connaître pour les différents langages de programmation en rapport avec le distributeur, des exemples par les codes, des notes, etc ...

Utilisation
-----------

### Dépendence

Disposer d'une installation de **Docker** fonctionnelle sur la machine pour pouvoir éxécuter ce projet.

Pour l'installation de **Docker** sous **Ubuntu** se reporter au dépôt suivant: [Ubuntu Docker installation](https://bitbucket.org/benjaminguysotteau/docker/src/master/),
puis éxécuter le script **install.sh** en tant que **super administrateur**:

```sh
sudo ./install.sh
```

### Éxécution

Lors d'une nouvelle installation de **Docker**, il faut ajouter **sudo** devant cette ligne de commande pour pouvoir l'éxécuter:

```sh
docker run -d --rm -p 8002:8000 -v ${PWD}:/docs squidfunk/mkdocs-material
```

!!! info
    Dans le script **install.sh** on ajoute l'utilisateur courant aux groupes nécessaires au fonctionnement de **Docker**
    mais cette modification ne sera effective que lors du **prochain login** sur la machine.
