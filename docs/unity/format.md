Format
===

Référence
---

1. [Unity 3D - Exported formats](https://docs.unity3d.com/Manual/3D-formats.html)
1. [zSpace - Model importation guide](https://support.zspace.com/hc/en-us/articles/115005081643-Studio-3D-Model-File-Importing-Guide)
1. [Answers unity - Import inventor files to unity](https://answers.unity.com/questions/43758/what-is-the-best-way-to-import-inventor-files-to-u.html)
