qtcreator.sh
===

Contenu
---

Les **images de VM** distribuées par <ins>PHYTEC</ins>, proposent l'alias **pqt** qui permet d'ouvrir **QtCreator**
en chargeant au préalable les **éléments nécessaires pour la compilation** à destination du module <ins>PHYTEC</ins>.

!!! tip "À vérifier"
	Le lancement du **QtCreator** avec l'alias **pqt** créer par <ins>PHYTEC</ins> permettrait en plus de **compiler** à destination du **module**
	
	mais aussi pour la version **desktop** (<ins>sans avoir besoin de quitter QtCreator</ins>), ce qui n'est pas possible
	
	dans l'autre sens.
	

L'adresse du répertoire.

```sh
/opt/x64_Qt5.3.2/Tools/QtCreator/bin
```

Le contenu du fichier exécuter par l'alias **pqt**.

```sh
. /opt/yogurt/AM335x-PD15.1.1/environment-setup-cortexa8t2hf-vfp-neon-phytec-linux-gnueabi

#! /bin/sh

makeAbsolute() {
    case $1 in
        /*)
            # already absolute, return it
            echo "$1"
            ;;
        *)
            # relative, prepend $2 made absolute
            echo `makeAbsolute "$2" "$PWD"`/"$1" | sed 's,/\.$,,'
            ;;
    esac
}

me=`which "$0"` # Search $PATH if necessary
if test -L "$me"; then
    # Try readlink(1)
    readlink=`type readlink 2>/dev/null` || readlink=
    if test -n "$readlink"; then
        # We have readlink(1), so we can use it. Assuming GNU readlink (for -f).
        me=`readlink -nf "$me"`
    else
        # No readlink(1), so let's try ls -l
        me=`ls -l "$me" | sed 's/^.*-> //'`
        base=`dirname "$me"`
        me=`makeAbsolute "$me" "$base"`
    fi
fi

bindir=`dirname "$me"`
libdir=`cd "$bindir/../lib" ; pwd`
LD_LIBRARY_PATH=$libdir:$libdir/qtcreator${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}
export LD_LIBRARY_PATH
exec "$bindir/qtcreator" ${1+"$@"}
```

Le reste du contenu disponible à la même adresse.


```sh
total 2680
drwxr-xr-x  4 root root   4096 Jun 19  2015 imports
drwxr-xr-x 11 root root   4096 Jun 19  2015 plugins
-rwxr-xr-x  1 root root 234016 Sep 12  2014 qbs
-rwxr-xr-x  1 root root  48200 Sep 12  2014 qbs-config
-rwxr-xr-x  1 root root  69696 Sep 12  2014 qbs-config-ui
-rwxr-xr-x  1 root root  23176 Sep 12  2014 qbs-qmltypes
-rwxr-xr-x  1 root root  85352 Sep 12  2014 qbs-setup-qt
-rwxr-xr-x  1 root root 188216 Sep 12  2014 qbs-setup-toolchains
drwxr-xr-x 16 root root   4096 Jun 19  2015 qml
-rwxr-xr-x  1 root root 854544 Sep 12  2014 qml2puppet
-rwxr-xr-x  1 root root 821136 Sep 12  2014 qmlpuppet
-rw-r--r--  1 root root     83 Sep 12  2014 qt.conf
-rwxr-xr-x  1 root root  78584 Sep 12  2014 qtcreator
-rwxr-xr-x  1 root root  14736 Sep 12  2014 qtcreator_process_stub
-rwxr-xr-x  1 root root   1042 Jul 10  2015 qtcreator.sh
-rwxr-xr-x  1 root root  48184 Sep 12  2014 qtpromaker
-rwxr-xr-x  1 root root 236696 Sep 12  2014 sdktool
```

___


Exécution
---

Pour exécuter **QtCreator** en chargeant les <ins>ressources nécessaires</ins> à la compilation du module <ins>PHYTEC</ins>.

1. Entrer la <ins>commande en</ins> étant sous **bash**.
1. Ajouter lors de la commande le `&` pour <ins>déplacer l'affichage</ins> de la sortie standard et la sortie d'erreur en dehors du terminal courant.


```sh
# On s'assure d'être sous bash
bash

# On se sert de l'alias et on modifie l'affichage de la sortie standard et celui de la sortie d'erreur
pqt &
```
