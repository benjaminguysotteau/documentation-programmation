Port série
==========

Ouverture, fermeture, lecteur et écriture d'un **port série** sous **Qt Creator** avec exemple par le code.

Initialisation
--------------

1. Créer une structure pour définir chacun des paramêtres du **Port Série**.
2. Déclarer un attribut de Classe de type **pointeur QSerialPort**.
3. Déclarer le **SLOT** (méthode) qui sera appelé lors d'une **erreur** sur le **Port Série**.

```c++
// cctalk_serial_port.hpp

#ifndef CCTALK_SERIAL_PORT_H
#define CCTALK_SERIAL_PORT_H

#include <QSerialPort>

// À améliorer avec une map ou un vector en remplacement de la structure.
struct s_serial
{
    QString port;
    QSerialPort::BaudRate baudRate;
    QSerialPort::DataBits dataBits;
    QSerialPort::Parity parity;
    QSerialPort::StopBits stopBits;
    QSerialPort::FlowControl flowControl;
};
```

```c++
private:
    QSerialPort *sp_cctalk;

public slots:
    void slt_serialPortHandleError(QSerialPort::SerialPortError error);

#endif // CCTALK_SERIAL_PORT_H
```

1. Définir le **Serial Port** dans le construteur de l'objet.
2. Initialiser chacun des paramètres du **Serial Port** à l'aide de la **structure** (ou de la **map** ou **vector** après amélioration).
2. Associer le **SIGNAL** du **Serial Port** avec le **SLOT** de la **méthode** appelée lorsque le **Serial Port** rencontre une erreur.

```c++
// cctalk_serial_port.cpp

CCTALKserialPort::CCTALKserialPort(struct s_serial *p_s_serial)
{
    sp_cctalk = new QSerialPort();

    sp_cctalk->setPortName(p_s_serial->port);
    sp_cctalk->setBaudRate(p_s_serial->baudRate);
    sp_cctalk->setDataBits(p_s_serial->dataBits);
    sp_cctalk->setParity(p_s_serial->parity);
    sp_cctalk->setStopBits(p_s_serial->stopBits);
    sp_cctalk->setFlowControl(p_s_serial->flowControl);

    connect(sp_cctalk, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(slt_serialPortHandleError(QSerialPort::SerialPortError)));

    qDebug() << "CctalkSerialPort::CctalkSerialPort port com cctalk : " << sp_cctalk->portName();
}
```

Éxécuter les différentes instructions lorsque une **erreur** sur le **Port Série** sera présente.

```c++
// cctalk_serial_port.cpp

void CCTALKserialPort::slt_serialPortHandleError(QSerialPort::SerialPortError error)
{
    if(error == QSerialPort::ResourceError)
    {
        qDebug() << sp_cctalk->errorString() << " Fermeture du port serie";
        sp_cctalk->close();
    }
}
```

Ouverture
---------

Déclarer une méthôde de Classe de type **bool**.

```c++
// cctalk_serial_port.hpp

public:
    bool is_serialPortOpen(void);
```

Définir la méthode de Classe.

```c++
// cctalk_serial_port.cpp

bool CCTALKSerialPort::is_serialPortOpen(void)
{
    if(sp_cctalk->open(QIODevice::ReadWrite) == 0)
    {
        qDebug() << "CCTALKserialPort::is_serialPortOpen() error: " << sp_cctalk->errorString();
        sp_cctalk->close();
        return(false);
    }
    return(true);
}
```

Vérifier que l'ouverture du **Port série** est un succès.

```c++
// widget.cpp
if( pCctalkSerial->openCctalkSerial() == false)
{
    QString err = tr("Erreur critique: ouverture port série CCTALK ");
    pStatus->fatalError( err);
}
```

Fermeture
---------

Depuis le destructeur de l'object, appeler la **méthode close** et **supprimer le pointeur** sur le **Port série**.

```c++
// cctalk_serial_port.cpp

CCTALKserialPort::~CCTALKserialPort()
{
    sp_cctalk->close();
    delete sp_cctalk;
}
```

Lecture
-------

Déclarer le **SLOT** (méthode) qui sera appelé lors de la **lecture** sur le **Port Série**.

```c++
// cctalk_serial_port.hpp

public slots:
    void slt_readDataOnSerialPort(void);
```
Associer le **SIGNAL** du **Serial Port** avec le **SLOT** de la **méthode** appelée lorsque le **Serial Port** est lu.

```c++
// cctalk_serial_port.cpp

CCTALKserialPort::CCTALKserialPort(struct s_serial *p_s_serial)
{
    connect(sp_cctalk, SIGNAL(readyRead()), this, SLOT(slt_readDataOnSerialPort()));
}
```

Éxécuter les différentes instructions lorsque la **lecture** du **Port Série** est **prêt à lire**.

```c++
// cctalk_serial_port.cpp

void CCTALKserialPort::slt_readDataOnSerialPort()
{
    QByteArray ba_read;

    ba_read += serial->readAll();
}
```

Lecture suite à un Timer
------------------------

1. Déclarer un **define** pour le temps **en millisecondes** du **Timer**.
2. Déclarer un attribut de Classe de type **pointeur QTimer**.
3. Déclarer le **SLOT** (méthode) qui sera appelé lors du **TimeOut** du **Timer**.

```c++
#ifndef CCTALK_SERIAL_PORT_H
#define CCTALK_SERIAL_PORT_H

#include <QTimer>

def_READ_DATA_TIMEOUT_IN_MS 10 // Toutes les 10 millisecondes.

private: 
    QTimer *tmr_readDataOnSerialPort;

public slot:
    void slt_readDataOnSerialPortTimeOut(void);

#endif // CCTAKL_SERIAL_PORT_H
```

1. Définir le **Timer** dans le construteur de l'objet en lui **passant l'instance courante** en paramêtre.
2. Associer le **SIGNAL** du **Timer** avec le **SLOT** de la **méthode** appelée lorsque celui ci tombe à zéro.
3. **Initialiser le compteur** du **Timer** en milliseconde(s), Qt Creator est plus ou moins précis entre 1 et 5 millisecondes.

```c++
// cctalk_serial_port.cpp

CCTALKserialPort::CCTALKserialPort()
{
    tmr_readDataFromBillReader = QTimer(this);

    connect(tmr_readDataFromBillReader, SIGNAL(timeout()), this, SLOT(slt_readDataFromBillReader()));
    tmr_readDataFromBillReader->start(READ_DATA_TIMEOUT_IN_MS);
}
```
1. **Arrêter** le **décompte** du **Timer**.
2. Éxécuter les différentes instructions lorsque le **compteur** du **Timer** est **tombé à zéro**.
3. **Réinitialiser** le **Timer**.

```c++
// cctalk_serial_port.cpp

void CCTALKserialPort::slt_readDataOnSerialPortTimeOut(void)
{
    tmr_readDataOnSerialPort->stop();

    emit(cctalkSerialRead(ba_read);
    ba_read.clear();

    tmr_readDataOnSerialPort->start(def_READ_DATA_TIMEOUT_IN_MS);
}
```

Écriture
--------

```c++
// cctalk_serial_port.hpp

public slots:
    void slt_writeDataOnSerialPort(QByteArray toWrite);
```
