GDB Serveur
===

Éxecuter le **binaire** en cours **développement** depuis la <ins>machine en locale</ins> à destination du module <ins>PHYTEC</ins> à travers une **connexion SSH**.

Permet d'être en **mode debug en live** directement sur la <ins>machine physique</ins> (versus émulateur sous OS).

___


Configuration
---

### Serveur

Sous **PTXDist** en version **15**, il n'y a <ins>rien à installer ou configurer</ins> car ca a été fait en amount par <ins>PHYTEC</ins>.

___


### Locale

Parcourir le menu <ins>Tools</ins> pour cliquer sur l'action **Options...**

Se déplacer dans le <ins>boîte de dialogue</ins> pour configurer le **debugguer** (<ins>GDB</ins>).

1. **Menu latéral** gauche
1. Sélectionner **Debugger**
1. Onglet **GDB**
1. Champ texte **Additional Attach Commands**
1. Ajouter les deux lignes ci-dessous.


On ajoute les **bibliothèques** dans les commandes additionnelles du compilateur pour ne pas avoir à envoyer toutes les **libraires** sur le module <ins>PHYTEC</ins>

mais utilisées celles qui sont <ins>appelées habituellement</ins> pour une compilation en locale:

```sh
set solib-absolute-prefix /opt/yogurt/AM335x-PD15.1.1/sysroots/cortexa8t2hf-vfp-neon-phytec-linux-gnueabi/usr/lib
```

On configure l'**environnement requis** pour le module <ins>PHYTEC</ins> lors de l'exécution:


```sh
set solib-search-path /opt/yogurt/AM335x-PD15.1.1/environment-setup-cortexa8t2hf-vfp-neon-phytec-linux-gnueabi
```

![Qt Tools Options](img/qt_tools_options.png)

![]()

___


Pré-requis
---

### Serveur

Se connecter en **SSH** au <ins>distributeur</ins> puis exécuter la commande suivante.

```sh
#gdbserver :<numero-de-port\> <binaire-à-exécuter\>
gdbserver  :10042             ./DISTRIBUTEUR
```

??? info "Résultat sortie standard"
	```sh
	root@phycore-am335x-1:/home# gdbserver :10042 ./DISTRIBUTEUR
	Process ./DISTRIBUTEUR created; pid = 2290
	Listening on port 10042
	```
	
	Un <ins>processus</ins> à été crée avec l'id **2290** et le <ins>serveur</ins> écoute sur le port **10042**.

___


### Locale

Lancer **QtCreator** via la commande `pqt &` ([plus d'informations sur la commande](qtcreator.md))

Pour pouvoir se connecter à **gdbserver**, il faut au préalable [lancer le binaire](#serveur_1) avec un <ins>numéro de port</ins> et le <ins>binaire</ins> en paramètre.

Parcourir le menu <ins>Debug</ins> pour cliquer sur l'action **Attach to Remote Debug Server...**

1. **Debug** (Menu supérieur)
1. **Start Debugging**
1. **Attach to Remote Debug Server...**

![Qt Debug menu](img/qt_debug_menu.png)


Dans le formulaire **Attach to Remote Debug Server...**, saisir les différents paramètres:

![Attach to Remote Debug Server](img/qt_attach_to_remote_debug_server.png)


| Champ                       | Valeur                       | Information                                                                                         |
| :---                        | :---                         | :---                                                                                                |
| **Kit**                     | phyBOARD-Wega                | Le **kit utilisé** à destination du module <ins>PHYTEC</ins> pour la compilation                    |
| **Server port**             | 10042                        | Le **port** affecté au serveur gdb                                                                  |
| **Override server address** | 192.168.0.15                 | L'**ip du distributeur** physique sur le réseau en intranet                                         |
| **Local executable**        | [...]/DISTRIBUTEUR           | Le **chemin complet** vers le binaire qui est le résultat de la compilation avec le kit sélectionné |
| **Recent**                  | DISTRIBUTEUR (phyBOARD-Wega) | Le **nom du binaire** avec le **kit sélectionné** entre parenthèses                                 |
	
___


Exécution
---

### Serveur

Une fois la **connection établie** on peut voir le `qDebug()` et autres fonctions qui écrivent sur la **sortie standard** et la **sortie d'erreur** s'afficher dans l'<ins>exécution de gdbserver</ins>.

??? info "Résultat sortie standard"
	```sh
	root@phycore-am335x-1:/home# gdbserver :10042 ./DISTRIBUTEUR
	Process ./DISTRIBUTEUR created; pid = 2355
	Listening on port 10042
	Remote debugging from host 192.168.0.13
	11:32:11.494 unknown Unable to query physical screen size, defaulting to 100 dpi.
	To override, set QT_QPA_EGLFS_PHYSICAL_WIDTH and QT_QPA_EGLFS_PHYSICAL_HEIGHT (in millimeters).
	11:32:11.635 unknown VirtualKeyboardInputContextPlugin::create:  "qtvirtualkeyboard"
	11:32:11.712 main APPLI COMPILEE POUR TOURNER SUR EMBARQUE PHYTEC
	11:32:11.788 Widget::Widget ==============================================================================================
	11:32:11.790 Widget::Widget ================= V1.33.1 =====================================================
	11:32:11.791 Widget::Widget ==============================================================================================
	11:32:11.792 Widget::Widget - Messages created ...
	11:32:11.801 EcranAccueil::EcranAccueil 
	EcranAccueil::EcranAccueil -----
	11:32:11.812 unknown QMetaObject::connectSlotsByName: No matching signal for on_pushBtnAnnulation_clicked()
	11:32:12.116 Widget::Widget - StackedWidget created ...
	11:32:12.118 DistribConfig::DistribConfig object DistribConfig created...
	11:32:12.120 Widget::Widget - Distribconfig created ...
	11:32:12.123 DistribStatus::DistribStatus object DistribStatus created...
	11:32:12.124 Widget::Widget - DistribStatus created ...
	11:32:12.125 Widget::Widget - DistribSystem created ...
	11:32:12.128 Widget::Widget Initialisation du bus can...
	id: 0x00000202 mask: 0x00000787
	id: 0x0000018a mask: 0x000007ff
	id: 0x00000382 mask: 0x00000787
	interface = can0, family = 29, type = 3, proto = 1
	11:32:12.142 Widget::initAppli Demarrage applicatif...
	11:32:12.143 cansend::run init du CAN...
	id: 0x00000000 mask: 0x00000407
	interface = can0, family = 29, type = 3, proto = 1
	11:32:12.155 unknown Could not parse application stylesheet
	11:32:12.402 Widget::initAppli C'est parti...
	11:32:12.585 unknown VirtualKeyboardInputContext::setFocusObject
	11:32:12.588 unknown VirtualKeyboardInputContext::setFocusObject
	11:32:12.627 unknown VirtualKeyboardInputContext::setFocusObject
	11:32:12.656 PeriphStatus::setError 
	Ecriture evenement erreur en base de donnee "Centrale OK"
	11:32:12.698 main widget show
	11:32:12.757 Widget::connectionCentrale 
	
	====================================================
	11:32:12.759 Widget::connectionCentrale ... CONNECTION CENTRALE ...
	11:32:12.760 Widget::connectionCentrale ====================================================
	
	
	11:32:12.768 DistribBase::connect ----------------------------------------------------
	11:32:12.846 DistribBase::connect s_config.db       =  "QMYSQL"
	11:32:12.846 DistribBase::connect s_config.hostname =  "192.168.0.19"
	11:32:12.847 DistribBase::connect config.dbName     =  "heurtauxv01"
	11:32:12.917 DistribBase::connect config.userName   =  "distribHX"
	11:32:12.981 DistribBase::connect config.password   =  "moussauto"
	11:32:13.065 DistribBase::connect ----------------------------------------------------
	11:32:13.163 PeriphStatus::setError 
	Ecriture evenement erreur en base de donnee "Base de donnee OK"
	11:32:13.166 Widget::connectionCentrale Base de donnée operationnelle, intialisation des péripheriques, en cour
	11:32:13.314 DistribBase::setIPAdrs "192.168.0.15"
	11:32:13.317 DistribBase::setIPAdrs "255.255.255.0"
	11:32:13.326 CctalkSerialPort::CctalkSerialPort CctalkSerialPort::CctalkSerialPort  port com cctalk :  "/dev/ttyO2"
	11:32:13.337 PileCCTALK::PileCCTALK creation PileCCTALK
	11:32:13.340 HopperJeton::HopperJeton HopperJeton::HopperJeton  adrs cctalk Hopper jeton :  4
	11:32:13.345 LecteurBillet::LecteurBillet LecteurBillet::LecteurBillet  adrs cctalk lecteur billet :  40
	11:32:13.356 unknown QIODevice::write: device not open
	11:32:13.365 unknown QIODevice::write: device not open
	11:32:13.386 PeriphStatus::setError 
	Ecriture evenement erreur en base de donnee "Terminal CB OK"
	11:32:13.393 PeriphStatus::setError 
	Ecriture evenement erreur en base de donnee "Lecteur Opticard OK"
	paramLog.txt                                                                                                                                            100% 3055     3.0KB/s   00:00    
	11:32:19.456 EcranErreur::to_surveillance =====> VERS ECRAN_ACCUEIL <=====
	11:32:19.459 unknown VirtualKeyboardInputContext::setFocusObject
	11:32:19.462 DistribSystem::moveLog === > Entree dans moveLog(), jour precedent =  12 dateJour 12
	11:32:19.464 DistribSystem::moveLog === > Sortie de moveLog(), jour precedent =  12 dateJour 12
	11:32:19.466 EcranAccueil::setFormInit 
	_____________________________________________________________________________________
	11:32:19.467 EcranAccueil::setFormInit -------------------------- Ecran d'accueil -------------------------------------------
	
	11:32:19.469 EcranAccueil::setFormInit 
	
	--------------------------------------------------------------------------------
	11:32:19.472 EcranAccueil::setFormInit 
	EcranAccueil::setFormInit -----
	11:32:19.473 EcranAccueil::setFormInit TIMER START
	11:32:19.474 EcranAccueil::setFormInit Entree de reinit video
	11:32:20.117 PeriphStatus::setError 
	Ecriture evenement erreur en base de donnee "Hopper jeton 1 OK"
	11:32:20.121 PeriphStatus::setLevel 
	Ecriture evenement changement de niveau "Jetons presents hopper1"
	11:32:20.487 PeriphStatus::setError 
	Ecriture evenement erreur en base de donnee "Imprimante OK"
	11:32:20.490 PeriphStatus::setLevel 
	Ecriture evenement changement de niveau "Papier imprimante present"
	11:32:20.494 PeriphStatus::setError 
	Ecriture evenement erreur en base de donnee "Distributeur carte OK"
	11:32:20.685 PeriphStatus::setError 
	Ecriture evenement erreur en base de donnee "Lecteur billet OK"
	```
___


### Locale

Une fois cliqué sur le <ins>bouton OK</ins> de la boîte de dialogue **Attach to Remote Debug Server**, la <ins>fenêtre suivante apparait</ins> devant un fichier source en assembleur.

Elle nous informe que le <ins>debugguer s'est arrété</ins> à son point d'entrée par défaut qui est la **fonction main()**.


![Qt debug signal received](img/qt_debug_signal_received.png)

Cliquer sur le <ins>bouton OK</ins>, puis appuyer sur **F5** ou sur l'**icône** en bas à gauche (représente une **flêche circulaire** sur fond vert).

!!! attention
	Pour le **distributeur**, il faut appuyer sur le <ins>bouton OK</ins> et enchaîner avec le <ins>bouton continue</ins>
	
	sinon, le **QIO::Device**, pour le <ins>cctalk</ins> et le <ins>bus can</ins> ne fonctionnement pas!

![Qt debug signal received continue](img/qt_debug_signal_received_continue.png)

Exemple de vue en mode **debug**.

![Qt debug runtime local](img/qt_debug_runtime_local.png)

___


Breakpoints
---

___


Watchpoints
---
