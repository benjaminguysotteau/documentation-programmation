Timer
=====

Réalisation d'un **timer** sous **Qt Creator** avec exemple par le code.

---

Création
----

1. Déclarer un **define** pour le temps **en millisecondes** du **Timer**.
2. Déclarer un attribut de Classe de type **pointeur QTimer**.
3. Déclarer le **SLOT** (méthode) qui sera appelé lors du <ins>TimeOut</ins> du **Timer**.

```c++
#include <QTimer>

def_READ_DATA_TIMEOUT_IN_MS 10 // Toutes les 10 millisecondes.

private: 
    QTimer *tmr_readDataFromBillReader;

public slot:
    void slt_readDataFromBillReader(void);
```

---

1. Définir le **Timer** dans le construteur de l'objet en lui <ins>passant l'instance courante</ins> en paramêtre.
2. Associer le **SIGNAL** du <ins>Timer</ins> avec le **SLOT** de la <ins>méthode</ins> appelée lorsque celui ci tombe à zéro.
3. **Initialiser le compteur** du **Timer** en millisecondes *(Qt Creator est plus ou moins précis entre 1 et 5 millisecondes)*.

```c++
LecteurBillet::LecteurBillet()
{
    tmr_readDataFromBillReader = QTimer(this);

    connect(tmr_readDataFromBillReader, SIGNAL(timeout()),
		    this, SLOT(slt_readDataFromBillReader()));
    tmr_readDataFromBillReader->start(READ_DATA_TIMEOUT_IN_MS);
}
```

!!! Attention "Attention: Timer->start() dans un constructeur"
	Selon les cas **ne pas mettre de tmr_<quelque-chose\> dans le constructeur** de l'objet,
	
	pour ne pas créer un <ins>SEGFAULT</ins> comme ca été le cas pour **tmr_quitter_ecran_assistance->start(**[...]**)**;
	
	dans le **constructeur** de <ins>EcranAssistance::EcranAssistance()</ins>.
	
	Mis **tmr_quitter_ecran_assistance->start(**[...]**)** dans <ins>EcranAssistance::SetFormInit()</ins> pour résoudre le problème.

___

1. **Arrêter** le **décompte** du **Timer**.
1. Éxécuter les différentes instructions lorsque le <ins>compteur</ins> du **Timer** est <ins>tombé à zéro</ins>.
1. **Réinitialiser** le **Timer**.


```c++
void LecteurBillet::slt_readDataFromBillReader(void)
{
    tmr_readDataFromBillReader->stop();

    // Les différentes instructions à éxécuter après que le timer sous tombé à zéro.
    if(! ba_readDataFromBillReader.isEmpty() && ba_readDataFromBillReader.at(0) == cctalk_adrs)
    {
        if(ba_savePrevious != ba_readDataFromBillReader)
            qDebug() << "LecteurBillet::slt_readDataFromBillReader: - ba_readDataFromBillReader: "
                     << ba_readDataFromBillReader.toHex();

        ba_savePrevious.clear();
        ba_savePrevious = ba_readDataFromBillReader;
    }

    tmr_readDataFromBillReader->start(READ_DATA_TIMEOUT_IN_MS);
}
```

Status
---

```c++
tmr_validatedBill->remainingTime();
```
