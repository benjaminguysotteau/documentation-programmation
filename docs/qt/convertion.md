Convertion
===

QString
---

Convertir un type <ins>natif du langage C++</ins> vers un `QString` de **Qt**.
___


### From uint/int

Pour convertir un `unsigned int` ou un `int` en `QString` on fait appel à la méthode membre **number()** appartenant à la classe <ins>QString</ins>.

```c++
// distrib_status.cpp

// On convertie station_id en QString.
QString::number(pConfig->s_config.station_id);
```

___


### To const char*

Pour convertir un `QString` en `const char*` on fait appel à la méthode membre **toStdString()** chaînant obligatoirement avec **c_str()** qui appartient à la classe <ins>QString</ins>.

```c++
// distrib_system.cpp

const QString message = "Proccess crash!";

// On convertie message en const char* .
qFatal(message.toStdString().c_str());
```

