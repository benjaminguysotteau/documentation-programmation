Debug
===

Les fonctions Qt pour <ins>afficher des messages</ins> sur les sorties correspondantes:

1. qDebug
1. qWarning
1. qCritical
1. qFatal

Si on utilise <ins>les méthodes ci dessus</ins> pour écrire des messages, il est possible de **définir un comportement** spécifique pour chaque fonction.

Pour cela il faut créer une variable d'environnement **QT_MESSAGE_PATTERN** et définir un comportement à l'aide des bloques de code disponibles.

!!! info "Exemple: nom de fonction en mode debug" 

    Pour un binaire compilé en mode **debug**, afficher le <ins>nom de la fonction</ins>:

    ```sh
    %{if-debug}
      %{function}
    %{endif}
    ```

    Ce qui donne pour **variable d'environnement**:
  
    ```sh
    export QT_MESSAGE_PATTERN="%{if-debug}%{function}%{endif}"
    ```

___

Debug plus explicite
--------------------

!!! tip "À ajouter au ~/.bashrc"
    Pour éviter de recréer la <ins>variable d'environnement</ins> à chaque fois, ajouter la ligne au **~/.bashrc** de la machine qui éxecute le binaire.

___

Ajouter de la couleur
---------------------

___

Finalement
----------

```sh
export QT_MESSAGE_PATTERN="`echo -e "\033[32m%{time h:mm:ss.zzz}%{if-category}\033[32m %{category}:%{endif} %{if-debug}\033[34m%{function}%{endif}%{if-warning}\033[31m%{backtrace depth=3}%{endif}%{if-critical}\033[31m%{backtrace depth=3}%{endif}%{if-fatal}\033[31m%{backtrace depth=3}%{endif}\033[0m %{message}"`"
```

___


Automatisation
---

Déclarer la variable d'environnment `QT_MESSAGE_PATTERN` au <ins>démarrage du distributeur</ins>.

Ajouter export QT_MESSAGE_PATTERN=[...] au fichier **/etc/profile**.

??? info "Exemple: Résultat sortie standard"
	```sh
	root@phycore-am335x-1:~# tail /etc/profile

	if [ -x /usr/bin/resize ];then
		/usr/bin/resize >/dev/null
	fi
	
	export PATH PS1 OPIEDIR QPEDIR QTDIR EDITOR TERM
	
	umask 022
	
	export QT_MESSAGE_PATTERN="`echo -e "\033[32m%{time h:mm:ss.zzz}%{if-category}\033[32m %{category}:%{endif} %{if-debug}\033[34m%{function}%{endif}%{if-warning}\033[31m%{function}%{endif}%{if-critical}\033[31m%{backtrace depth=3}%{endif}%{if-fatal}\033[31m%{backtrace depth=3}%{endif}\033[0m %{message}"`"
	root@phycore-am335x-1:~#
	```
	
!!! attention
	Mettre la variable d'environnement `QT_MESSAGE_PATTERN` dans **/home/root/.bashrc** ou dans ne fonctionne pas!

___


Référence
---------

1. [Woboq nice debug](https://woboq.com/blog/nice-debug-output-with-qt.html)
