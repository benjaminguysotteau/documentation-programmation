Processus
===

Exécuter un <ins>processus externe</ins> depuis un **binaire Qt**.

___


System
---

`system` fait partie de la **bibliothèque standard C++**, on peut l'utiliser sous Qt notamment si n'a <ins>pas besoin d'avoir le retour</ins> de la commande.

```c++
// system("<la-commande-a-executer>");

system("reboot");
```
___


Process
---

`process` fait partie de la **bibliothèque Qt**, il est indispensable quand on a <ins>besoin de connaître le retour</ins> de la commande.

___


### Déclaration

Exemple de **déclaration** avec DistribStatus::watchBinaryStatusProcess().

```c++
// distrib_status.h

#include <QProcess>

public:
	void watchBinaryStatusProcess(const QString &fullPathCommand);
```

___


### Définition

Exemple de **définition** avec DistribStatus::watchBinaryStatusProcess().

!!! info "Ce que fait DistribStatus::watchBinaryStatusProcess("/usr/bin/egtouchd")"
	On surveille l'état du binaire **eGTouchD** (gestion de l'écran tactile) contenu dans <ins>/usr/bin</ins> depuis son processus.
	
	Lorsque celui-ci <ins>n'est plus exécuter</ins> ou qu'un évenement externe la mis <ins>en pause</ins>, on **redémarre** le distributeur.
	
	
??? warning "Utilisation des pipes de redirections à la ligne 6"
	Pour la ligne 6:
	
	```sh
	QString command = "ps | grep -i " + fullPathCommand + " | grep -v grep";
	```
	La commande est **composée de pipes**, il ne faut <ins>pas exécuter l'instruction</ins> comme suit:
	
	```sh
	processResult = process.execute(command);
	```
	
	Sinon le **binaire ps** nous retourne le <ins>message d'erreur</ins> suivant:
	
	```sh
	ps: invalid option -- '|'
	```


```c++
void DistribStatus::watchBinaryStatusProcess(const QString &fullPathCommand)
{
    QProcess process;
    int processResult;

    QString command = "ps | grep -i " + fullPathCommand + " | grep -v grep";

    processResult = process.execute("sh", QStringList() << "-c" << command);
    // Process cannot be started.
    if(processResult == -2)
    {
        QString message = "Process cannot be started!";
        qFatal(message.toStdString().c_str());
        appliLog.write("DistribStatus::watchBinaryStatusProcess(): ", message);
    }
    // Process crashed.
    else if(processResult == -1)
    {
        QString message = "Proccess crashed!";
        qFatal(message.toStdString().c_str());
        appliLog.write("DistribStatus::watchBinaryStatusProcess(): ");
    }
    // Process successed, return code external process.
    else
    {
        /* if grep return 0, process currently running,
        *  otherwise (return 1) process doesn't exist.
        */
        if(processResult != 0)
        {
            const QString str_message = "eGTouch daemon doesn't run anymore!";
            qWarning() << str_message;
            appliLog.write("DistribStatus::watchBinaryStatusProcess(): ", str_message);
			system("reboot");
        }
    }
}
```

| Paramètre     | Valeur    | Information                                                                                              |
| :---          | :---      | :---                                                                                                     |
| processResult | **-2**    | Le processus ne <ins>pas réussi à s'exécuter</ins>                                                       |
| processResult | **-1**    | Le processus à <ins>crashé</ins> lors de l'exécution                                                     |
| processResult | **autre** | Toutes les autres valeurs sont celles <ins>retournées par la commande</ins> exécuté depuis le process Qt |

___


### Appel

Exemple d'**appel** de méthode avec DistribStatus::watchBinaryStatusProcess().

```c++
// ecran_accueil.cpp

extern class DistribStatus *pStatus;

pStatus->watchBinaryStatusProcess("/usr/bin/egtouchd");
```

___


Process without QStringList
---

### Déclaration

La [même déclaration](#declaration) que pour une implémentation avec QStringList.

___


### Définition

```c++
void DistribStatus::executeExternalProcess(const QString &command)
{
    QProcess process;
    int processResult;

	// On exécute directement la commande avec les charactères spéciaux échappés lors de l'appel.
    processResult = process.execute(command);

    // Process cannot be started.
    if(processResult == -2)
    {
        const char *message = "Process cannot be started!";
        qFatal(message);
        appliLog.write("DistribStatus::executeExternalProcess(): ", message);
    }
    // Process crash.
    else if(processResult == -1)
    {
        const char *message = "Proccess crash!";
        qFatal(message);
        appliLog.write("DistribStatus::executeExternalProcess(): ");
    }
    // Return code process.
    else
    {
        /* if grep return 0, process currently running,
        *  otherwise (return 1) process doesn't exist.
        */
        if(processResult != 0)
        {
            const QString str_message = "eGTouch daemon doesn't run anymore!";
            qWarning() << str_message;
            appliLog.write("DistribStatus::executeExternalProcess(): ", str_message);
			system("reboot");
        }
    }
}
```
___


### Appel

```c++
// ecran_accueil.cpp

extern class DistribStatus *pStatus;

pStatus->executeExternalProcess("/bin/sh -c \"ps | grep -i /usr/bin/egtouchd | grep -v grep \"");
```

!!! tip "Explication passage de paramètres"
	On met l'<ins>intégralité de la commande</ins> que l'on veut exécuter **entre guillemets** puis on échappe ces mêmes guillemets et on passe le tout à **/bin/sh -c** ou **/bin/bash -c**.


Référence
---

1. [doc.qt.io - QProcess](https://doc.qt.io/qt-5/qprocess.html#execute-1)
1. [Stackoverflow - How to run a system command in Qt](https://stackoverflow.com/questions/3227767/how-to-run-a-system-command-in-qt)
1. [Stackoverflow - Piping or command chaining with QProcess](https://stackoverflow.com/questions/20901884/piping-or-command-chaining-with-qprocess)
1. [forum.qt.io - How to pipe process](https://forum.qt.io/topic/85066/how-to-pipe-qprocess-stdout-to-father-process-as-stdin)
